﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SeatAssignerCore;

namespace SeatAssigner
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        Arrangement arrangement;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            arrangement = Suseo.arrangement;
            SeatLayout.UpdateActionDelegate updateAction = () => {
                totalSeatsLabel.Content = arrangement.TotalSeats;
                usableSeatsLabel.Content = arrangement.UsableSeats;
                pairsAvailableLabel.Content = arrangement.PairsAvailable(false);
            };

            updateAction();
            SeatLayout.initGrid(seatLayoutGrid, updateAction, arrangement);
        }

        private void runButton_Click(object sender, RoutedEventArgs e)
        {
            ArrangementModule.clear(arrangement);

            pairResultTextBox.Text = "";
            soloResultTextBox.Text = "";

            IEnumerable<Tuple<string, string>> pairs;
            try
            {
                pairs = ArrangementModule.assignPairsNET(int.Parse(pairsTextBox.Text), arrangement);
            }
            catch (InvalidOperationException)
            {
                pairResultTextBox.Text = "합석 가능 인원 초과";
                return;
            }
            catch (FormatException)
            {
                soloResultTextBox.Text = "2인 합석 쌍에는 숫자만";
                return;
            }

            foreach (var pair in pairs)
            {
                pairResultTextBox.Text = pairResultTextBox.Text + pair.ToString() + "\n";
            }

            var solos = ArrangementModule.assignSoloNET(arrangement);
            foreach (var solo in solos)
            {
                soloResultTextBox.Text = soloResultTextBox.Text + solo + "\r\n";
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            ArrangementModule.clear(arrangement);
        }

        private void soloCopyButton_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(soloResultTextBox.Text);
        }

        private void pairCopyButton_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(pairResultTextBox.Text);
        }
    }
}
