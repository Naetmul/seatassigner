﻿namespace SeatAssignerCore

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Controls.Primitives
open System.Windows.Data

module SeatLayout =
    type UpdateActionDelegate = delegate of unit -> unit

    type SeatStatusToColorConverter() =
        interface IValueConverter with
            member this.Convert(value, targetType, parameter, culture) =
                let status = value :?> SeatStatus
                match status with
                | SeatStatus.Empty -> Media.Brushes.LightGreen :> obj
                | SeatStatus.Occupied -> Media.Brushes.Pink :> obj
                | SeatStatus.UnusableEmpty -> Media.Brushes.White :> obj
                | SeatStatus.UnusableOccupied -> Media.Brushes.DarkGray :> obj

            member this.ConvertBack(value, targetType, parameter, culture) = null

    let consecutiveSeatCells row columnStart columnEnd =
        let count = columnEnd - columnStart + 1
        Array.init count (fun i -> Some(Seat(row, columnStart + i, SeatStatus.Empty)))

    let consecutiveEmptyCells count =
        Array.create count None

    let initGrid (grid : Grid) (updateAction: UpdateActionDelegate) (arrangement : Arrangement) =
        let chart = arrangement.Chart
        let rowNumber = Array2D.length1 chart
        let columnNumber = Array2D.length2 chart

        for i in 1 .. rowNumber do
            let row = RowDefinition()
            row.Height <- GridLength(1.0, GridUnitType.Star)
            grid.RowDefinitions.Add(row)
        
        for i in 1 .. columnNumber do
            let column = ColumnDefinition()
            column.Width <- GridLength(1.0, GridUnitType.Star)
            grid.ColumnDefinitions.Add(column)
        
        Array2D.iteri (fun i j (cell : Seat option) ->
            match cell with
            | Some seat ->
                let btn = Button()
                btn.Content <- seat.Name

                btn.Click.Add(fun evArgs ->
                    Seat.toggleUsability seat
                    updateAction.Invoke())

                let backgroundBinding = Binding("Status")
                backgroundBinding.Source <- seat
                backgroundBinding.Converter <- SeatStatusToColorConverter()
                btn.SetBinding(Button.BackgroundProperty, backgroundBinding) |> ignore

                Grid.SetRow(btn, i)
                Grid.SetColumn(btn, j)
                grid.Children.Add(btn) |> ignore
            | None -> ()
        ) chart

module Suseo =
    open SeatLayout

    let arrangement =
        let chartT1 : Seat option array list list = [
            [consecutiveSeatCells 1 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 1 13 23; consecutiveEmptyCells 1]
            [consecutiveSeatCells 2 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 2 13 24]
            [consecutiveSeatCells 3 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 3 13 24]
            [consecutiveSeatCells 4 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 4 13 24]
            [consecutiveSeatCells 5 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 5 13 24]
            [consecutiveSeatCells 6 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 6 13 24]
            [consecutiveSeatCells 7 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 7 13 24]
            [consecutiveSeatCells 8 1 12; consecutiveEmptyCells 1; consecutiveSeatCells 8 13 24]
            [consecutiveEmptyCells 16; consecutiveSeatCells 9 16 21; consecutiveEmptyCells 3]
            [consecutiveEmptyCells 18; consecutiveSeatCells 10 18 20; consecutiveEmptyCells 4] ]
        let chartT2 : Seat option array list = chartT1 |> List.map (fun arrayList -> Array.concat arrayList)
        let chartT3 : Seat option array array = List.toArray chartT2
        let chart = Array.jaggedTo2D chartT3;

        let unusableSeats = [
            "F11"; "F12"; "F13"; "F14";
            "G11"; "G12"; "G13"; "G14";
            "H10"; "H11"; "H12"; "H13"; "H14"; "H15";
            "I16"; "I17"; "I18"; "I19"; "I20"; "I21";
            "J18"; "J19"; "J20"]

        unusableSeats |> List.iter (fun s ->
            chart |> Seq.cast<Seat option>
                |> Seq.choose id
                |> Seq.filter (fun seat -> seat.Name = s)
                |> Seq.iter (fun seat -> seat.Status <- SeatStatus.UnusableEmpty))

        Arrangement(chart)