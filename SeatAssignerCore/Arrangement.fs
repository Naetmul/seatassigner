﻿namespace SeatAssignerCore

type SeatingChart = Seat option[,]

type Arrangement(chart : SeatingChart) =
    member this.Chart = chart

    member this.TotalSeats = Seq.cast<Seat option> chart |> Seq.filter (fun x -> x.IsSome) |> Seq.length
    member this.UsableSeats = Seq.cast<Seat option> chart |> Seq.filter (function Some seat -> seat.IsUsable | None -> false) |> Seq.length
    member this.EmptySeats = Seq.cast<Seat option> chart |> Seq.filter (function Some seat -> seat.Status = SeatStatus.Empty | None -> false) |> Seq.length

    member this.PairsAvailable (exceptOccupied: bool) =
        let pairsInRow = [
            for i in 0 .. (Array2D.length1 chart - 1) do
                let rowList = chart.[i, *] |> Array.toList
                let rec countPairs cnt (list : Seat option list) =
                    match list with
                    | Some seat1 :: Some seat2 :: tail ->
                        if exceptOccupied then
                            if seat1.Status = SeatStatus.Empty && seat2.Status = SeatStatus.Empty then countPairs (cnt + 1) tail
                            else countPairs cnt (Some seat2 :: tail)
                        else
                            if seat1.IsUsable && seat2.IsUsable then countPairs (cnt + 1) tail
                            else countPairs cnt (Some seat2 :: tail)
                    | _ :: tail -> countPairs cnt tail
                    | [] -> cnt
                yield countPairs 0 rowList
        ]
        pairsInRow |> List.sum

module Arrangement =
    let random = Random.random

    let clear (arrangement : Arrangement) =
        let chart = arrangement.Chart
        for i in 0 .. (Array2D.length1 chart - 1) do
            for j in 0 .. (Array2D.length2 chart - 1) do
                match chart.[i, j] with
                | Some seat ->
                    match seat.Status with
                    | SeatStatus.Empty | SeatStatus.UnusableEmpty -> ()
                    | SeatStatus.Occupied | SeatStatus.UnusableOccupied -> seat |> Seat.toggleOccupancy
                | None -> ()

    let rec private assignPairs pairs (arrangement : Arrangement) : (string * string) list option =
        if pairs = 0 then Some []
        elif arrangement.PairsAvailable true < pairs then None
        else
            let chart = arrangement.Chart

            let row = random.Next(0, Array2D.length1 chart)
            let column1 = random.Next(0, Array2D.length2 chart - 1)
            let column2 = column1 + 1

            let inline retry () = assignPairs pairs arrangement

            match (chart.[row, column1], chart.[row, column2]) with
            | (Some seat1, Some seat2) ->
                if seat1.Status = SeatStatus.Empty && seat2.Status = SeatStatus.Empty then
                    Seat.toggleOccupancy seat1
                    Seat.toggleOccupancy seat2
                    
                    let result = assignPairs (pairs - 1) arrangement
                    match result with
                    | Some list -> Some ((seat1.Name, seat2.Name) :: list)
                    | None ->
                        Seat.toggleOccupancy seat1
                        Seat.toggleOccupancy seat2
                        retry ()
                else
                    retry ()
            | (_, _) -> retry ()

    let private assignSolo (arrangement : Arrangement) : string list =
        let chart = arrangement.Chart

        let available =
            chart |> Seq.cast<Seat option>
                |> Seq.filter (function Some seat -> seat.Status = SeatStatus.Empty | None -> false)
                |> Seq.choose id
                |> Seq.map (fun seat -> seat.Name)
        
        let array = available |> Seq.toArray 

        Array.shuffle array

        Array.toList array

    let assignPairsNET pairs (arrangement : Arrangement) : (string * string) seq =
        let result = assignPairs pairs arrangement
        match result with
        | Some list -> list |> List.toSeq
        | None -> raise (System.InvalidOperationException())

    let assignSoloNET (arrangement: Arrangement) : string seq =
        assignSolo arrangement |> List.toSeq