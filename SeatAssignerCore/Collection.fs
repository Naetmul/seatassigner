﻿namespace SeatAssignerCore

module Array =
    let shuffle (a: 'T array) : unit =
        let length = a.Length
        let swappingPairs = [0 .. (length-2)] |> List.map (fun i -> (i, Random.random.Next(i, length)))

        swappingPairs |> List.iter (
            fun (i, j) ->
                let temp = a.[i]
                a.[i] <- a.[j]
                a.[j] <- temp )

    let jaggedTo2D (jagged : 'T array array) : 'T[,] =
        Array2D.init jagged.Length jagged.[0].Length (fun i j -> jagged.[i].[j])
