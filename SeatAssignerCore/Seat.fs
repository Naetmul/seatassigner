﻿namespace SeatAssignerCore

module SeatNaming =
    let rowNameOf (row : int) : string =
        assert (1 <= row && row <= 26)
        let letter = char (int 'A' + (row - 1))
        string letter

    let columnNameOf (column : int) : string =
        assert (column >= 1)
        string column

    let nameOf (row : int) (column : int) : string =
        rowNameOf row + columnNameOf column

    let rowOf (rowName : string) : int =
        assert (rowName.Length = 1)
        let rowNameLetter = rowName.[0]
        assert('A' <= rowNameLetter && rowNameLetter <= 'Z')
        int rowNameLetter - int 'A' + 1

    let columnOf (columnName: string) : int =
        int columnName

    let rowColumnOf (name: string) : int * int =
        (rowOf name.[0..0], columnOf name.[1..])

type SeatStatus =
    | Empty
    | Occupied
    | UnusableEmpty
    | UnusableOccupied

[<StructuredFormatDisplay("Seat {name}")>] // for printf "%A" Seat
type Seat(row : int, column : int, status : SeatStatus) =
    let mutable status = status

    let event = Event<_, _>()

    interface System.ComponentModel.INotifyPropertyChanged with
        [<CLIEvent>]
        member this.PropertyChanged = event.Publish

    member this.Row = row
    member this.Column = column
    member this.Name = SeatNaming.nameOf row column

    member this.Status
        with get () = status
        and set (value) =
            status <- value
            event.Trigger(this, System.ComponentModel.PropertyChangedEventArgs("Status"))

    member this.IsUsable = (status = SeatStatus.Empty) || (status = SeatStatus.Occupied)
    member this.Unusable = (status = SeatStatus.UnusableEmpty) || (status = SeatStatus.UnusableOccupied)

module Seat =
    let toggleUsability (seat : Seat) =
        match seat.Status with
        | SeatStatus.Empty -> seat.Status <- SeatStatus.UnusableEmpty
        | SeatStatus.Occupied -> seat.Status <- SeatStatus.UnusableOccupied
        | SeatStatus.UnusableEmpty -> seat.Status <- SeatStatus.Empty
        | SeatStatus.UnusableOccupied -> seat.Status <- SeatStatus.Occupied

    let toggleOccupancy (seat : Seat) =
        match seat.Status with
        | SeatStatus.Empty -> seat.Status <- SeatStatus.Occupied
        | SeatStatus.Occupied -> seat.Status <- SeatStatus.Empty
        | SeatStatus.UnusableEmpty -> seat.Status <- SeatStatus.UnusableOccupied
        | SeatStatus.UnusableOccupied -> seat.Status <- SeatStatus.UnusableEmpty